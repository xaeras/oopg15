
public interface ConsumerInterface extends SimulationPartInterface {
    //Zieht dem Storage Content ab, wenn value <= content und Zeit vorkommt
    //int value > 0; 0 <= int time < simulationTime;
    void useStorage(int value, int time);
}
