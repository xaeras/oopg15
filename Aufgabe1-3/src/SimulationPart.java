import java.util.ArrayList;


 public abstract class SimulationPart implements SimulationPartInterface {

   private ArrayList<StorageInterface> storageContainer = new ArrayList<>();

    // 0 <= int time < simulationtime;
    abstract String toString(int time);

    //"StorageInterface out" != null;
    //entfernt "StorageInterface out" aus dem jeweiligen storageContainer;
   void storageDelete(StorageInterface out){
      storageContainer.remove(out);
   }

    //storageContainer != null;
    //retourniert den jeweiligen storageContainer;
   public ArrayList<StorageInterface> getStorageContainer(){
      return storageContainer;
   }


     public void storageAdd(StorageInterface in){
         storageContainer.add(in);
     }
}
