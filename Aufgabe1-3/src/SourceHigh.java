/**
 * Created by marwinschindler on 21.11.17.
 */
public class SourceHigh extends Source{

    private int count = 0;

    public SourceHigh(String name, int duration, double chance, int simulationTime) {
        super(name, 10, duration, chance, simulationTime);
    }

    public void fillStorage(int value, int time) {
        if(containsTime(time)){
            for (int i = 0; i<super.getStorageContainer().size(); i++){
                this.getStorageContainer().get(i).putInStorage((int)((1d/getStorageContainer().size()) * 10 + 0.5), time);
                count += (int)((1d/getStorageContainer().size()) * 10 + 0.5);
            }
        }
    }

    private boolean containsTime(int time){
        if(getTimes().get(time)){
            return true;
        }
        return false;
    }

    public String toString(){
        return "i bims SourceHigh und han " + count + " vong Produktion her.";
    }

}

