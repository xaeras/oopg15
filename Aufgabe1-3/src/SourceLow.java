import java.util.BitSet;

/**
 * Created by marwinschindler on 15.11.17.
 */
public class SourceLow extends Source {

    private int count= 0;

    public SourceLow(String name, int duration, double chance, int simulationTime) {
        super(name, 3, duration, chance, simulationTime);
    }

    public void fillStorage(int value, int time) {
        if(containsTime(time)){
            for (int i = 0; i<super.getStorageContainer().size(); i++){
                this.getStorageContainer().get(i).putInStorage((int)((1d/getStorageContainer().size()) * 3 + 0.5), time);
                count += (int)((1d/getStorageContainer().size()) * 3 + 0.5);
            }
        }
    }

    private boolean containsTime(int time){
        if(getTimes().get(time)){
            return true;
        }
        return false;
    }

    public String toString(){
        return "i bims SourceLow und han " + count + " vong Produktion her.";
    }

}
