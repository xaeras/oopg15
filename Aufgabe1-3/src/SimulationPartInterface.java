import java.util.ArrayList;

/**
 * Created by marwinschindler on 15.11.17.
 */
public interface SimulationPartInterface {

    // 0 <= int time < simulationTime;
    void minuteTask(int time);

    //"Simulationpart in" != null;
    //fuegt "Simulationpart in" zu dem jeweiligen storageContainer hinzu;
    void storageAdd(StorageInterface in);

    ArrayList<StorageInterface> getStorageContainer();

}
