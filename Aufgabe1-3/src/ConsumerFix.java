import java.util.ArrayList;


public class ConsumerFix extends Consumer {

    private int[][] times;

    //0 <= int[][] time < simulationtime (es sollte kein Zeitwert außerhalb der simulierten Zeit liegen)
    //0 <= intensity
    public ConsumerFix(int[][] times, int intensity){
        this.times = times;
        this.intensity = intensity;
    }


    //ueberpruefen ob gegebene Zeit vorkommt
    //0 <= int time < simulationtime
    boolean containsTime(int time){
        for(int i = 0; i < times.length; i++){
                if(times[i][0] <= time && time <= times[i][1]){
                    return true;
                }
        }
        return false;
    }

    //Fuehrt useStoarge durch
    //BAD: Klassenzusammenhang, Methode minutetTask ist redundant
    public void minuteTask(int time) {
        useStorage(intensity, time);
    }

    @Override

    //Gibt null aus
    //NOTE: toString ist hier redundant
    public String toString(int time) {
        return null;
    }

    @Override


    //Zieht dem Storage Content ab, wenn value <= content und Zeit vorkommt
    //int value > 0; 0 <= int time < simulationTime;
    public void useStorage(int value, int time) {
        if(containsTime(time)){
            for (int i = 0; i<getStorageContainer().size(); i++){
                getStorageContainer().get(i).putInStorage((int)((1d/getStorageContainer().size()) * intensity + 0.5), time);
                //FEHLER: es gehoert putOutOfStorage dorthin, wurde offensichtlich verwechselt
            }
        }
    }


}
