import java.util.ArrayList;
import java.util.BitSet;


public class ConsumerRandom extends Consumer {

    private BitSet times;
    private int duration;
    private double chance;
    private int simulationsTime;


    //0 <= intensity
    //0 <= duration
    //0 <= chance
    public ConsumerRandom(int intensity, int duration, double chance, int simulationsTime){
        this.intensity = intensity;
        this.duration = duration;
        this.chance = chance/simulationsTime;
        this.simulationsTime = simulationsTime;

        this.times = init(this.chance,duration); //errechnete Zeiten des Consumers

        this.times = ausfall(this.times, this.chance, simulationsTime/100); //Zeiten nun mit technischen Ausfaellen
    }

    /**
     * NOTE: Checks the duration of the Simulation and initializes the timetable for the Consumer.
     */

    //GOOD: Klassenzusammenhang, Die zufaellige Erzeugung der Zeiten hat zwei Schritte (Erzeugung und Ausfaelle berechnen) und benoetigt dabei
    //einige Variablen zur Zusammenarbeit (Wahschenilichkeit, Laenge, Simulationslaufzeit, Zeiten-Bitset)

    private BitSet init(double chance, int duration){
        BitSet zeiten = new BitSet(simulationsTime);
        for(int i = 0; i < simulationsTime; i++){
            if (Math.random() < (chance*(simulationsTime /(simulationsTime -(duration*(chance* simulationsTime)))))){
                zeiten.set(i, i+duration);
                i += duration;
            }
        }
        return zeiten;
    }


    private BitSet ausfall(BitSet zeiten, double chance, int lentgh){
        for (int i = 0; i < simulationsTime; i++) {         //NOTE: geht alle Zeieinheiten durch
            if (containsTime(i)){                           //NOTE: sucht die, wo der Consumer aktiv ist
                if((Math.random()*(chance*100))<chance){    //NOTE: mit einer 1% Wsl. der Chance vom Consumer faellt dieser aus (evtl. spaeter mit eigenger Chance berechnen)
                    zeiten.set(i, i + lentgh);
                    i += lentgh;
                }
            }
        }
        return zeiten;
    }

    //ueberpruefen ob gegebene Zeit vorkommt
    //0 <= int time < simulationtime
    boolean containsTime(int time){
        if(times.get(time)){
            return true;
        }
        return false;
    }

    //Fuehrt useStoarge durch
    //BAD: Klassenzusammenhang, Methode minutetTask ist redundant.
    public void minuteTask(int time) {
        useStorage(intensity, time);
    }

    @Override

    //Gibt null aus
    //NOTE: toString ist hier redundant
    public String toString(int time) {
        return null;
    }

    @Override

    //Zieht dem Storage Content ab, wenn value <= content und Zeit vorkommt
    //int value > 0; 0 <= int time < simulationTime;
    public void useStorage(int value, int time) {
        if(containsTime(time)){
            for (int i = 0; i<getStorageContainer().size(); i++){
                getStorageContainer().get(i).putOutOfStorage((int)((1d/getStorageContainer().size()) * intensity + 0.5), time);
            }
        }
    }

}