import java.util.ArrayList;
import java.util.BitSet;


public class Source extends SimulationPart implements SourceInterface{

    public BitSet getTimes() {
        return times;
    }

    private BitSet times;
    private String name;

    public int getIntensity() {
        return intensity;
    }

    private int intensity;
    private int productivity;
    private int simulationsTime;
    private int duration;
    private double chance;
    //GUT: Objekt-Koppelung, Jede Variable der Klasse ist "private", da kein Zugriff von außen geschehen soll.

    //int intensity > 0; int duration > 0; double chance > 0; int simulationTime > 0;
    public Source(String name, int intensity, int duration,double chance, int simulationTime) {
        this.name = name;
        this.intensity = intensity;
        this.simulationsTime = simulationTime;
        this.chance = chance/simulationTime;
        this.duration = duration;

        this.times = init(this.chance, duration);
        this.times = ausfall(this.times, this.chance, simulationTime/100);
    }

    private BitSet init(double chance, int duration){
        BitSet zeiten = new BitSet(simulationsTime);
        for(int i = 0; i < simulationsTime -duration; i++){
            if (Math.random() < (chance*(simulationsTime /(simulationsTime -(duration*(chance* simulationsTime)))))){
                zeiten.set(i, i+duration);
                i += duration;
            }
        }
        return zeiten;
    }

    private BitSet ausfall(BitSet zeiten, double chance, int lentgh){
        for (int i = 0; i < simulationsTime; i++) {
            if (containsTime(i)){
                if((Math.random()*(chance*200))<chance){
                    zeiten.set(i, i + lentgh);
                    i += lentgh;
                }
            }
        }
        return zeiten;
    }

    private boolean containsTime(int time){
            if(times.get(time)){
                return true;
            }
        return false;
    }

    //ruft "fillStorage" auf;
    //0 <= int time < simulationtime; int intensity > 0;
    public void minuteTask(int time) {
        fillStorage(intensity, time);
    }
    //SCHLECHT: Klassen-Zusammenhang, die Methode minuteTask ist redundan und koennte ohne Aenderungen im Programmablauf hervorzurufen entfernt werden;

    //gibt "String name" und "int productivity" in der Konsole aus;
    public String toString() {
        return new String("Die Erzeugung von " + name+ " betraegt: " + productivity);
    }

    //gibt "String name" und "int productivity" zu der angegeben Zeit in der Konsole aus;
    //0 <= int time < simulationtime;
    public String toString(int time) {
        return new String("Die Erzeugung von " + name + " zur Zeit " + (time / 60)%24 + ":" + (time % 60) + " Uhr Tag "+ ((time/1440)+1) + " betraegt: " + productivity);
    }

    //wenn das Bit mit dem Index "int time" in "Bitset times" TRUE ist,
    //wird productivity/(Anzahl Storages in Storagelist) zu dem Inhalt von jedem Storage in der Storagelist hinzugefuegt;
    //int value > 0; 0 <= int time < simulationtime; 0 <= int productivity < intensity;
    public void fillStorage(int value, int time) {
        if(containsTime(time)){
            productivity = (int)(Math.random() * intensity);
            for (int i = 0; i<super.getStorageContainer().size(); i++){
                this.getStorageContainer().get(i).putInStorage((int)((1d/getStorageContainer().size()) * productivity + 0.5), time);
            }
        }
    }
}

