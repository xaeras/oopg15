
public interface StorageInterface extends SimulationPartInterface {

    // BAD: unnoetige dynamische Bindung. Verursacht, dass interne Funktion public ist. Kein anderes Objekt soll auf diese Klasse zugreifen.
    // value wird zum content addiert -> Nachbedingung;
    // int value >= 0; 0 <= int time < simulationtime -> Vorbedingung;
    int putInStorage(int value, int time);

    // BAD: unnoetige dynamische Bindung. Verursacht, dass interne Funktion public ist. Kein anderes Objekt soll auf diese Klasse zugreifen.
    // value wird vom content abgezogen -> Nachbedingung;
    // int value >= 0; 0 <= int time < simulationtime -> Vorbedingung;
    int putOutOfStorage(int value, int time);

    //0 <= int col <= 4;
    double getColSum(int col);

    //0 <= int col <= 4; 0 <= int fromIndex < int simulationTime - 1; int fromindex < int toIndex < int simulationTime;
    double getColSum(int col, int fromIndex,int toIndex);

}
