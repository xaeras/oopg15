/**
 * Created by andre on 25.10.2017.
 */
public interface SourceInterface extends SimulationPartInterface {

    //int value > 0; 0 <= int time < simulationtime;
    void fillStorage(int value, int time);
}
