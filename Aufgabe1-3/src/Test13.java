/**
 * Created by andikao on 15.10.17.
 */
public class Test13 {

    public static void main(String[] args) {
//        System.out.println("\n" + "1. Simulation" + "\n");
//        //Storages, Sources, Consumers, Time
//        System.out.println(Simulation.start(1,1,1,10000));
//        System.out.println("\n" + "2. Simultaion" + "\n");
//        System.out.println(Simulation.start(3,2,5,10000));
        System.out.println(Simulation.start(3,3,3,1000));
    }
}

/*

Arbeitsaufteilung Aufgabe 1:

Arbeitsbeginn zu Dritt

Grundgerüst Andi und Tobi

Zufalls Consumer und Source Marwin, Berechnung dafür Tobi

Storageklasse explitzit Andi

Feinschliff zu Dritt


Aufgrund von Startschwierigkeiten mit GIT mussten wir am Anfang enger zusammen arbeiten, was die klare Aufteilung etwas unübersichtlich macht.


Arbeitsaufteilung Aufgabe 2:
Andi:   Implementierung der Möglichkeit von Hierachien von Storages.
        Heißt auch, dass Storages jetzt als Consumer und Source für andere Storages dienen können.
        Zur kurzen Veranschaulichung in Simulation2.start ein kleines Beispiel. Da gibt es aber noch Nachholbedarf beim Kombinieren mit der richtigen Simulation.java Datei.
Tobi:   Simulieren von Transportkosten und Schwund des Inhalts durch Alterung und technische Gebrechen.
Marwin: Dynamische Verbindungen zwischen den Klassen.
        Mehrere Quellen, Speicher, Consumer innerhalb einer Simulation.
        Random Test (Test+Simulation-Klasse)

Arbeitsaufteilung Aufgabe 3:
Andi: Kommentare in Storage, StorageInterface, Simulation2.
Tobi: Kommentare in ConsumerInterface, Consumer, CosumerRandom, ConsumerFix.
Marwin: Kommentare in Simulation, Simulationpart, Source, Sourceinterface.
 */
