public class Simulation {

    //erstellt "int storage" mal Storages, "int consumer" mal Cosumer und "int sources" mal Sources,
    //fuegt jeder Source und jedem Consumer x Storages zu der jeweiligen Storageliste hinzu; 1 <= x <= "int storages"
    //String ausgabeStorage != ""; String intensitySource != ""; String durationSource != ""; String chanceSource != ""; String intensityConsumer != ""; String durationConsumer != "";String chanceConsumer != ""; int storagesubstitutions >= 0;
    //gibt "ausgabeStorage, intensitySource, durationSource, chanceSource, intensityConsumer, durationConsumer, chanceConsumer, storagesubstitutions" in der Konsole aus;

    //wenn ein Storage A weniger Zufuhr als Verbrauch hat und ein anderer Storage B mehr Zufuhr als Verbrauch wird, wenn es einen Consumer C gibt in dessen Storageliste Storage A ist, auch Storage B zu der Storageliste von Consumer C hinzugefuegt;
    //wenn ein Storage A keine Zufuhr hat und  ein anderer Storage B mehr Zufuhr als Verbrauch wird, wenn es eine Source C gibt in dessen Storageliste Storage B ist, auch Storage A zu der Storageliste von Source C hinzugefuegt;

    //int storages > 0; int sources > 0; int consumers > 0; int simulationtime > 0;
    public static String start(int storages, int sources, int consumers, int simulationtime){

        StorageInterface[] simulationStorages= new Storage[storages];
        SourceInterface[] simulationSources = new Source[sources];
        ConsumerInterface[] simulationConsumers= new ConsumerRandom[consumers];


        String ausgabeStorage = "";

        String intensitySource = "";
        String durationSource = "";
        String chanceSource = "";

        String intensityConsumer = "";
        String durationConsumer = "";
        String chanceConsumer = "";

        for (int i = 0; i < storages; i++){
            int temp = (int)(Math.random()*1000); ausgabeStorage += temp + ", ";

            StorageInterface storage = new Storage(temp, 0, simulationtime);
            simulationStorages[i] = storage;
        }


        for (int i = 0; i < sources-2; i++){
            int tempIntensity = (int)(Math.random()*100); intensitySource += tempIntensity + ", ";
            int tempDuration = (int)(Math.random()*1000); durationSource += tempDuration + ", ";
            int tempChance = (int)(Math.random()*20); chanceSource += tempChance + ", ";

            SourceInterface source = new Source("Source", tempIntensity, tempDuration, tempChance, simulationtime);
            simulationSources[i] = source;
            simulationSources[i].storageAdd(simulationStorages[(int)(Math.random()*storages)]);
            for (int x = 0; x < storages; x++){
                if (Math.random() < 0.5){
                    simulationSources[i].storageAdd(simulationStorages[x]);
                }
            }
        }
        
        SourceLow sourceLow = new SourceLow("SourceLow", 20, 20, simulationtime);
        simulationSources[sources-2] = sourceLow;
        simulationSources[sources-2].storageAdd(simulationStorages[(int)(Math.random()*storages)]);
        intensitySource += sourceLow.getIntensity() + ", ";
        durationSource += 20 + ", ";
        chanceSource += 20 + ", ";


        SourceHigh sourceHigh = new SourceHigh("SourceHigh", 20, 20, simulationtime);
        simulationSources[sources-1] = sourceHigh;
        simulationSources[sources-1].storageAdd(simulationStorages[(int)(Math.random()*storages)]);
        intensitySource += sourceHigh.getIntensity() + ", ";
        durationSource += 20 + ", ";
        chanceSource += 20 + ", ";


        for (int i = 0; i < consumers; i++){
            int tempIntensity = (int)(Math.random()*50); intensityConsumer += tempIntensity + ", ";
            int tempDuration = (int)(Math.random()*500); durationConsumer += tempDuration + ", ";
            int tempChance = (int)(Math.random()*10); chanceConsumer += tempChance + ", ";

            ConsumerInterface consumer = new ConsumerRandom(tempIntensity, tempDuration, tempChance,simulationtime);
            simulationConsumers[i] = consumer;
            simulationConsumers[i].storageAdd(simulationStorages[(int)(Math.random()*storages)]);

            for (int x = 0; x < storages; x++){
                if (Math.random() < 0.5){
                    simulationConsumers[i].storageAdd(simulationStorages[x]);
                }
            }
        }

        int storageSubstitutions = 0;

        for (int i = 0; i<simulationtime; i++){

            for (int x = 0; x<sources; x++){
                simulationSources[x].minuteTask(i);
            }

            for (int x = 0; x<consumers; x++){
                simulationConsumers[x].minuteTask(i);
            }

            for (int x = 0; x<storages; x++){
                simulationStorages[x].minuteTask(i);
            }


            if (i>20) {
                for (int x = 0; x < storages; x++) {
                    if (simulationStorages[x].getColSum(2, i, (i - 10)) > 0) {
                        for (int y = 0; y < storages; y++){
                            if (simulationStorages[y].getColSum(1,i,i-10)>100 ){
                                for(int z = 0; z < consumers; z++){
                                    if (simulationConsumers[z].getStorageContainer().contains(simulationStorages[x])){
                                        simulationConsumers[z].storageAdd(simulationStorages[y]);
                                        storageSubstitutions++;
                                    }
                                }
                            }
                        }
                        if (simulationStorages[x].getColSum(4, i, (i- 10)) == 0){
                            for (int y = 0; y < storages; y++) {
                                if (simulationStorages[y].getColSum(1, i, i - 10) > 100) {
                                    for (int z = 0; z < sources; z++){
                                        if (simulationSources[z].getStorageContainer().contains(simulationStorages[y])) {
                                            simulationSources[z].storageAdd(simulationStorages[x]);
                                            storageSubstitutions++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /*
            Anmerkung:
            A = checks if the quantity of units in a Storage x where to little for the usage;
            B = checks if the quantity of units in another Storage y where to much for the usage;
            if (A && B) -> C = checks for a Consumer z that takes units from Storage x;
            if (C) -> add Storage y to the Storagelist of Consumer z;
            D = checks if the supply of the Storage x = 0;
            if (D && B) -> E = checks for a Source z that produces units for Storage y;
            if (E) -> add Storage x to Storagelist of Source z;
            */
        }

        String ausgabe = "";

        ausgabe+= storages + " Storages:" + "\n" + "Kapazitaeten: " + ausgabeStorage + "\n\n" +

                sources + " Sources:" + "\n" + "Intensity:     " + intensitySource + "\n" + "Duration(min): " + durationSource + "\n" + "Chance/Day:    " + chanceSource + "\n\n" +

                consumers + " Consumers:" + "\n" + "Intensity:     " + intensityConsumer + "\n" + "Duration(min): " + durationConsumer + "\n" + "Chance/Day:    " + chanceConsumer + "\n\n";

        for (int i = 0; i<storages; i++) {
            ausgabe+= "Storage " + (i+1) + "\n" + "øVerbrauch/min: " + simulationStorages[i].getColSum(3)/simulationtime + " øZufur/min: " + simulationStorages[i].getColSum(4)/simulationtime + " øInhalt: " + simulationStorages[i].toString() + "\n\n";
        }

        ausgabe += "Anzahl an Storage wechsel: " + storageSubstitutions + "\n";

        ausgabe += "\n" + sourceHigh.toString() + "\n" + sourceLow.toString();

        return ausgabe;

    }

}
