import java.util.ArrayList;


public class Storage extends SimulationPart implements StorageInterface, ConsumerInterface, SourceInterface{
//GUT: Dynamisches Binden, mit jedem Interface wird die Funktionalitaet von dem demjenigen Simulationspart implementiert, da der Storage jede dieser Funktionalitaeten aufweisen muss.

    private final int capacity;
    private int content;

    private int [][] documentation; // NOTE: [x][y] x -> point of time; y -> 0=content, 1=overflow, 2=more energy needed, 3=use/minute, 4=supply/minute;
    private int simulationtime;

    //int capacity > 0; 0 <= int content <= capacity; int simulationtime > 0; ArrayList storageContainer != null;
    public Storage(int capacity, int content, int simulationstime){
        this.capacity = capacity;
        this.content = content;
        this.documentation = new int[simulationstime][5];
        this.simulationtime = simulationstime;
    }

    // BAD: Namenskonvention ist zu aehnlich zu der MEthode, die durch SourceInterface implementiert werden muessen
    // value wird zum content addiert -> Nachbedingung;
    // int value >= 0; 0 <= int time < simulationtime -> Vorbedingung;
    public int putInStorage(int value, int time){
        int random = 5 + (int)(Math.random()*5);

        value = (value*100) - (value*random);

        value = value/100;

        documentation[time][4] = value;
        if(capacity - content == 0) {
            if(getStorageContainer() != null && getStorageContainer().size()!= 0){
                fillStorage(value, time);
            }
            documentation[time][1] = value;
            return value;
        } else if(capacity - (content + value) < 0){
            value =  content + value - capacity;
            content = capacity;
            if(getStorageContainer() != null && getStorageContainer().size()!= 0){
                fillStorage(value, time);
            }
            documentation[time][1] = value;
            return value;
        } else {
            content = content + value;
        }
        return 0;
    }

    // value wird zum content addiert -> Nachbedingung;
    // int value >= 0; 0 <= int time simulationtime -> Vorbedingung;
    public void fillStorage(int value, int time) {
        for(int i = 0; value != 0 && i < getStorageContainer().size(); i++){
            value = getStorageContainer().get(i).putInStorage(value, time);
        }
    }

    // BAD: Namenskonvention ist zu aehnlich zu der MEthode, die durch SourceInterface implementiert werden muessen
    // value wird vom content abgezogen -> Nachbedingung;
    // int value >= 0; 0 <= int time < simulationtime -> Vorbedingung;
    public int putOutOfStorage(int value, int time) {
        int random = 5 + (int)(Math.random()*15);

        value = (value*100) + (value*random);
        value = value/100;

        documentation [time][3] = value;
        if(content == 0) {
            if(getStorageContainer() != null && getStorageContainer().size()!= 0){
                useStorage(time, value);
            }
            documentation[time][2] = value;
            return value;
        } else if(content - value < 0){
            value =  value - content;
            content = 0;
            if(getStorageContainer() != null && getStorageContainer().size()!= 0){
                useStorage(time, value);
            }
            documentation[time][2] = value;
            return value;
        } else {
            content = content - value;
        }
        return 0;
    }

    // value wird vom content abgezogen -> Nachbedingung;
    // int value >= 0; 0 <= int time < simulationtime -> Vorbedinung;
    public void useStorage(int value, int time) {
        for(int i = 0; value != 0 && i < getStorageContainer().size(); i++){
            value = getStorageContainer().get(i).putOutOfStorage(value, time);
        }
    }

    /* BAD: mit minuteTask und den anderen Interfacespezifischen Methoden existiert Redundanz, die nicht sein sollte.
    minuteTask sollte das konstante Werken der Simulationsteile simulieren und soll nur von einer Simulations-Klasse
    aufgerufen werden. Hier in dieser Klasse wird nur dokumentiert und Alterung simuliert, aber das unterscheidet sich sehr von der Implementierung der anderen Klassen.
     */
    // die Aufgabe fuer die bestimmte Uhrzeit wurde ausgefuehrt -> Nachbedinung
    public void minuteTask(int time){

        if (Math.random()<=(100/simulationtime)) {
            content = content - (content / 100);
        }
        documentation[time][0] = content;
    }

    // gibt aussagekraeftige Daten aus -> Nachbedinung
    public String toString(int time) {
        return toString();
    }

    // FEHLER: Unnoetige Ueberbleibsel
    private int getContent() {
        return content;
    }
    // FEHLER: Unnoetige Ueberbleibsel
    private int getContent(int time) {
        return documentation[time][0];
    }

    // 0 <= col < documentation.length -> Vorbedingung
    public double getColSum(int col){
        int sum = 0;
        for(int i = 0; i<documentation.length; i++){
            sum += documentation[i][col];
        }
        return sum;
    }

    // 0 <= col < documentation[0].length && 0 <= fromIndex <= toIndex <= documentation.length -> Vorbedingung
    public double getColSum(int col, int fromIndex,int toIndex){
        int sum = 0;
        for (int i = fromIndex; i >= toIndex ; i--){
            sum+=documentation[i][col];
        }
        return sum;
    }

    // gibt aussagekraeftige Daten aus -> Nachbedinung;
    public String toString() {
        return ( (int)(getColSum(0)/simulationtime) + ", Ueberschuss: " + (int)(getColSum(1)) + ((getStorageContainer() != null && getStorageContainer().size()!= 0) ? " (der, wenn moeglich, in einen darueberliegenden Storage eingespeißt wurde.)" : "") + ", mussten hinzugefuegt werden: " + (int)(getColSum(2))) + ((getStorageContainer() != null && getStorageContainer().size()!= 0) ? " (sofern moeglich von einem darueberliegenden Storage entnommen.)" : "");
    }

}
