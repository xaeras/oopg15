/**
 * Created by andre on 21.11.2017.
 */
public interface EnergyGenerator extends Unit{

    // average supply of energy in kWh per year
    // energyOutput >= 0
    int energyOutput();

    // average use of electricity in kWh per year
    // energyInput >= 0
    int energyInput();

}
