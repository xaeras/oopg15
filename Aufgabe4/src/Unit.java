/**
 * Created by andre on 21.11.2017.
 */
public interface Unit {
    int investmentCosts(); // in Euro
    int runningCosts(); // average in Euro per year
    int energyOutput(); // average in kWh per year
    int energyInput(); // average in kWh per year
}
