/**
 * Created by andre on 21.11.2017.
 */
public class HeatPump implements HeatingSystem, EnergyGenerator{

    // investmentCosts >= 0;
    // investment costs in Euro
    public int investmentCosts(){
        return 0;
    }

    // runningCosts >= 0;
    // average running costs in Euro per year
    public int runningCosts(){
        return 0;
    }

    // energyInput >= 0;
    // input: electrical energy
    // average electrical energy input in kWh per year
    public int energyInput(){
        return 0;
    }

    // energyOutput >= 0;
    // output: thermal energy
    // average thermal energy output in kWh per year
    public int energyOutput(){
        return 0;
    }
}
