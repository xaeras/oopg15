/**
 * Created by andre on 21.11.2017.
 */
public interface ElectricPowerUnit extends Unit{

    // Rates the quality of the electrity (e.g. availability)
    int quality();
}
