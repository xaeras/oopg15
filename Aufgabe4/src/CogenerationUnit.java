/**
 * Created by andre on 21.11.2017.
 */
public class CogenerationUnit implements ElectricPowerUnit, HeatingSystem{

    // investmentCosts >= 0;
    // investment costs in Euro
    public int investmentCosts(){
        return 0;
    }

    // runningCosts >= 0;
    // average running costs in Euro per year
    public int runningCosts(){
        return 0;
    }


    // energyInput >= 0;
    // input: convential energysources (coal, fuels)
    // average electrical energy input in kWh per year
    public int energyInput(){
        return 0;
    }

    // energyOutput >= 0;
    // output: electricity or thermal
    // average energy output in kWh per year (minus waste heat)
    public int energyOutput(){ return 0; }

    // Rates the quality of the electrity (e.g. availability)
    public int quality() {return 0;}
}
