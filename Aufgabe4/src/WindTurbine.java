/**
 * Created by andre on 21.11.2017.
 */
public class WindTurbine implements ElectricPowerUnit, EnergyGenerator{

    // returns investment costs in Euro
    // investmentCosts >= 0
    public int investmentCosts(){return 0;}

    // returns average running costs in Euro per year
    // runningCosts >= 0
    public int runningCosts(){return 0;}

    // returns average electricity use
    // in kWh per year
    // energyInput >= 0
    public int energyInput(){return 0;}

    // returns average electricity supply
    // in kWh per year
    // energyOutput >= 0
    public int energyOutput(){return 0;}

    // Rates the quality of the electrity (e.g. availability)
    public int quality() {return 0;}
}
