/**
 * Created by andre on 21.11.2017.
 */
public class Test4 {
    public static void main(String[] args) {

        CogenerationUnit cogenerationUnit = new CogenerationUnit();

        ElectricPowerUnit electricPowerUnit = new WindTurbine();
        electricPowerUnit = new PhotovoltaicSystem();
        electricPowerUnit = cogenerationUnit;

        HeatingSystem heatingSystem = new SolarThermalSystem();
        heatingSystem = new HeatPump();
        heatingSystem = cogenerationUnit;

        EnergyGenerator energyGenerator = new HeatPump();
        energyGenerator = new PhotovoltaicSystem();
        energyGenerator = new SolarThermalSystem();
        energyGenerator = new WindTurbine();

        Unit unit = electricPowerUnit;
        unit = heatingSystem;
        unit = energyGenerator;
    }

    /*  Keine Untertypbeziehung zwischen CogenerationUnit und:
           EnergyGenerator:
              Hier herrscht Keine Untertypenbeziehung, weil die CogenerationUnit Energie einerseits Bedarfsorientiert Energie gewinnt und EnergyGenerator Keine 
              Angabe dazu hat (was noch Keine Untertypbeziehung EnergyGenerator>CogenerationUnit verneint, das kommt naemlich erst jetzt), andererseits kann es 
              umgekehrt auch Keine Beziehung geben, da die Inputs sich (meiner Meinung nach) ausschließen. Bei CogenerationUnit werden Konventionelle 
              Energieformen (Kohle, Erdoel, Erdgas) zu Elektrischer Energie oder Waerme verarbeitet, waehrend es sich bei EnergyGenerator um "frei verfuegbaren 
              Energiequellen" (Wind, Licht, Waerme des Umfelds) handelt.

           HeatPump:
              Hier herrscht meiner Meinung nach schon Keine Untertypbeziehung, weil CogenerationUnit explizit Bedarfsorientiert Energie abgeben 
              kann und HeatPump eben nicht. Zusaetzlich dazu ist die Energiequelle "Umgebungswaerme" (von HeatPump) Keine der "konventionellen 
              Energiequellen", die bei der CogenerationUnit gefordert wird.

           PhotovoltaicSystem:
              (Wie HeatPump) Hier herrscht meiner Meinung nach schon Keine Untertypbeziehung, weil CogenerationUnit explizit Bedarfsorientiert 
              Energie abgeben kann und PhotovoltaicSystem eben nicht. Zusaetzlich dazu ist die Energiequelle "Licht" (von PhotovoltaicSystem) Keine 
              der "konventionellen Energiequellen", die bei der CogenerationUnit gefordert wird.

           SolarThermalSystem:
              (Wie HeatPump) Hier herrscht meiner Meinung nach schon Keine Untertypbeziehung, weil CogenerationUnit explizit Bedarfsorientiert 
              Energie abgeben kann und SolarThermalSystem eben nicht. Zusaetzlich dazu ist die Energiequelle "Licht" (von SolarThermalSystem) 
              Keine der "konventionellen Energiequellen", die bei der CogenerationUnit gefordert wird.

           WindTurbine:
              (Wie HeatPump) Hier herrscht meiner Meinung nach schon Keine Untertypbeziehung, weil CogenerationUnit explizit Bedarfsorientiert 
              Energie abgeben kann und WindTurbine eben nicht. Zusaetzlich dazu ist die Energiequelle "Wind" (von WindTurbine) Keine der 
              "konventionellen Energiequellen", die bei der CogenerationUnit gefordert wird.

       Keine Untertypbeziehung zwischen ElectricPowerUnit und:
           EnergyGenerator:
              Hier herrscht meiner Meinung nach Keine Untertypbeziehung, weil auf der einen Seite EnergyGenerator explizit Energie aus 
              "frei verfuegbare Energiequellen" erzeugt (speziell), waehrend es bei ElectricPowerUnit eine beliebige Energiequelle sein 
              koennte, andererseits ist die gewonnene Energie bei der ElectricPowerUnit explizit elektrisch, waehrend es nun bei EnergyGenerator 
              nur allgemein in "Nutzbarer Form" generiert wird. Somit ist die Ersetzbarkeit in beide Richtungen nicht gegeben.

           HeatingSystem:
              Hier ist Keine Untertypbeziehung anzufinden, da sich die erzeugte Energieart gegenseitig ausschließt: ElectricPowerUnit>Elektrizitaet, 
              HeatingSyste>Thermale Energie. Bis auf diesen Punkt waere hier eine Untertypbeziehung ElectricPowerUnit<--HeatingSystem vorhanden.

           HeatPump:
              Keine Untertypbeziehung, da HeatPump thermale und ElectricPowerunit elektrische Energie ausgibt. Außerdem bekommt eine ElectricPowerunit 
              allgemein handelbare Energie als Input. Ein HeatPump Objekt waer somit nicht dort anwendbar, wo ein ElectricPowerunit Objekt gebraucht waere.

           SolarThermalSystem:
              Keine Untertypbeziehung, da SolarThermalSystem thermale und ElectricPowerunit elektrische Energie ausgibt. 
              Außerdem bekommt eine ElectricPowerunit   allgemein handelbare Energie als Input. Ein SolarThermalSystem Objekt 
              waere somit nicht dort anwendbar, wo ein ElectricPowerunit Objekt gebraucht waere.

       Keine Untertypbeziehung zwischen HeatingSystem und:
           ElectricPowerUnit:
              Keine Verbindung, da HeatPump thermale und ElectricPowerunit elektrische Energie ausgibt. Außerdem bekommt eine 
              ElectricPowerunit allgemein handelbare Energie als Input. Ein HeatPump Objekt waer somit nicht dort anwendbar, 
              wo ein ElectricPowerunit Objekt gebraucht waere.

           EnergyGenerator:
              Hier herrscht meiner Meinung nach Keine Untertypbeziehung, weil auf der einen Seite HeatingSystem bedarfsorientiert 
              Energie zur Verfuegung stellen kann, EnergyGenerator aber nicht (Was eine Untertypbeziehung in die andere Richtung 
              nicht ausschließt: EnergyGenerator<-HeatingSystem, aber dazu jetzt ein Grund dagegen). HeatingSystem generiert thermale 
              Energie aus "handelbarer Energie", waehrend EnergyGenerator "frei verfuegbare" Energiequellen benutzt, um diese in eine 
              Nutzbare Form umzuwandeln.

           PhotovoltaicSystem:
             Da HeatingSystem Energie in Form von Waerme liefert und PhotovolaticSystem in Form von Elektrizitaet.
             HeatingSystem benoetigte handelbare Energie PhotovolaticSystem im Gegenzug frei verfuegbare Energie
             und zusaetzlich Elekrtizitaet.

           WindTurbine:
             Keine Untertypbeziehung, da HeatingSystem Energie in Form von Waerme liefert und WindTurbine in Form von Elektrizitaet.
             HeatingSystem benoetigte handelbare Energie WindTurbine im Gegenzug frei verfuegbare Energie und zusaetzlich Elekrtizitaet.
             Dadurch waere ein Objekt vom Typ WindTurbine nicht mehr anwendbar wo ein Objekt vom Typ Heatingsystem erwartet wird.


        Keine Untertypbeziehung zwischen WindTurbine und:

            HeatingSystem :
                Keine Untertypbeziehung, da HeatingSystem Energie in Form von Waerme liefert und WindTurbine in Form von Elektrizitaet.
                HeatingSystem benoetigte handelbare Energie WindTurbine im Gegenzug frei verfuegbare Energie und zusaetzlich Elekrtizitaet.
                Dadurch waere ein Objekt vom Typ WindTurbine nicht mehr anwendbar wo ein Objekt vom Typ Heatingsystem erwartet wird.

            CogenerationUnit :
                Keine Untertypbeziehung, da CogenerationUnit Energie in Form von Waerme als auch Elektrizitaet erzeugt und WindTurbine nur als Elektrizitaet.
                Ein Objekt CogenerationUnit operiert bedarfsabhaengig, ein Objekt vom Typ WindTurbine nicht kann dafuer aber einen Speicher haben.
                CogenerationUnit benoetigte handelbare Energie WindTurbine frei verfuegbare Energie und zusaetzlich Elekrtizitaet.
                Dadurch koennte ein Objekt CogenerationUnit nicht von einem mit dem Typen WindTurbine ersetzt werden.

            HeatPump :
                Keine Untertypbeziehung, da HeatPump thermale Energie und ein Objekt vom Typ WindTurbine elektrische Energie ausgibt.
                Dadurch koennte ein Objekt HeatPump nicht von einem mit dem Typen WindTurbine ersetzt werden.

            SolarThermalSystem :
                Analog zu HeatPump

            PhotovolaticSystem :
                Ein Objekt vom Typ PhotovolaticSystem implemtiert die selben Obertypen wie eines vom Typ WindTurbine,
                daher stehen sie hirachisch gesehen auf der selben Stufe und eine Untertypbeziehungsbeziehung wuerde Keinen Sinn machen.


        Keine Untertypbeziehung zwischen PhotovolaticSystem und:

            HeatingSystem :
                Da HeatingSystem Energie in Form von Waerme liefert und PhotovolaticSystem in Form von Elektrizitaet.
                HeatingSystem benoetigte handelbare Energie PhotovolaticSystem im Gegenzug frei verfuegbare Energie
                und zusaetzlich Elekrtizitaet.

            CogenerationUnit :
                Keine Untertypbeziehung, da CogenerationUnit Energie in Form von Waerme als auch Elektrizitaet erzeugt und PhotovolaticSystem nur als Elektrizitaet.
                Ein Objekt CogenerationUnit operiert bedarfsabhaengig, ein Objekt vom Typ PhotovolaticSystem nicht kann dafuer aber einen Speicher haben.
                CogenerationUnit benoetigte handelbare Energie PhotovolaticSystem frei verfuegbare Energie und zusaetzlich Elekrtizitaet.
                Dadurch koennte ein Objekt CogenerationUnit nicht von einem mit dem Typen PhotovolaticSystem ersetzt werden.

            HeatPump :
                Keine Untertypbeziehung, da HeatPump thermale Energie und ein Objekt vom Typ PhotovolaticSystem elektrische Energie ausgibt.
                Dadurch koennte ein Objekt HeatPump nicht von einem mit dem Typen PhotovolaticSystem ersetzt werden.

            SolarThermalSystem :
                Analog zu HeatPump

            WindTurbine :
                Ein Objekt vom Typ PhotovolaticSystem implemtiert die selben Obertypen wie eines vom Typ WindTurbine,
                daher stehen sie hirachisch gesehen auf der selben Stufe und eine Untertypbeziehungsbeziehung wuerde Keinen Sinn machen.


        Keine Untertypbeziehung zwischen EnergyGenerator und:

            HeatingSystem:
                HeatingSystem operiert bedarfsorientiert, EnergyGenerator nicht.
                EnergyGenerator und HeatingSystem sind hierachisch gesehen gleichgestellt.
                HeatingSystem benoetigt handelbare Energie als Input, EnergyGenerator frei verfuegbare.
                EnergyGenerator liefert allgemein Energie, HeatingSystem im speziellen thermale Energie.

            ElectricPowerUnit:
                EnergyGenerator und ElectricPowerUnit sind hirachisch gesehen gleichgestellt.
                ElectricPowerUnit benoetigt handelbare Energie als Input, EnergyGenerator frei verfuegbare.

            CogenerationUnit:
                Ein Objekt vom Typ CogenerationUnit liefert gleichzeitig thermale Energie als auch Elektrizitaet, eines vom Typ EnergyGenerator nicht.
                EnergyGenerator benoetigt als Input elektrische Energie, CogenerationUnit hingegen allgemein Energie.



        Keine Untertypbeziehung zwischen HeatPump und:

        CogenerationUnit:
            Da HeatPump thermale und CogenerationUnit zusaetzlich elektrische Energie ausgibt.
            Ein HeatPump Objekt waer somit nicht dort anwendbar, wo ein CogenerationUnit Objekt gebraucht waere.

        ElectricPowerUnit:
            Da HeatPump thermale und ElectricPowerunit elektrische Energie ausgibt.
            Außerdem bekommt eine ElectricPowerunit allgemein handelbare Energie als Input.
            Ein HeatPump Objekt waer somit nicht dort anwendbar, wo ein ElectricPowerunit Objekt gebraucht waere.

        PhotovoltaicSystem:
            Da HeatPump thermale und PhotovoltaicSystem elektrische Energie ausgibt.
            Ein HeatPump Objekt waer somit nicht dort anwendbar, wo ein PhotovoltaicSystem Objekt gebraucht waere.

        SolarThermalSystem:
            Da HeatPump und SolarThermalSystem beide gemeinsam in HeatingSystem Untertypen sind und
            als unterschieldiche Arten davon gesehen werden.

        WindTurbine:
            Da HeatPump thermale und WindTurbine elektrische Energie ausgibt.
            Ein HeatPump Objekt waer somit nicht dort anwendbar, wo ein WindTurbine Objekt gebraucht waere.



        Keine Untertypbeziehung zwischen SolarThermalSystem und:

        CogenerationUnit:
            Da SolarThermalSystem thermale und CogenerationUnit zusaetzlich elektrische Energie ausgibt.
            Ein SolarThermalSystem Objekt waer somit nicht dort anwendbar, wo ein CogenerationUnit Objekt gebraucht waere.

        ElectricPowerUnit:
            Da SolarThermalSystem thermale und ElectricPowerunit elektrische Energie ausgibt.
            Außerdem bekommt eine ElectricPowerunit allgemein handelbare Energie als Input.
            Ein SolarThermalSystem Objekt waer somit nicht dort anwendbar, wo ein ElectricPowerunit Objekt gebraucht waere.

        HeatPump:
            Da SolarThermalSystem und HeatPump beide gemeinsam in HeatingSystem Untertypen sind
            und als unterschieldiche Arten davon gesehen werden.

        PhotovoltaicSystem:
            Da SolarThermalSystem thermale und PhotovoltaicSystem elektrische Energie ausgibt.
            Ein SolarThermalSystem Objekt waer somit nicht dort anwendbar, wo ein PhotovoltaicSystem Objekt gebraucht waere.

        WindTurbine:
            Da SolarThermalSystem thermale und WindTurbine elektrische Energie ausgibt.
            Ein SolarThermalSystem Objekt waer somit nicht dort anwendbar, wo ein WindTurbine Objekt gebraucht waere.


     */

    /*
    Arbeitsaufteilung Aufgabe 4:

    Andreas: Unit, CogenertionUnit, ElectricPowerUnit, HeatingSystem
             sowie Zugehoerige Erklaerungen in Test5.java

    Marwin: EnergyGenerator, PhotovolaicSystem, WindTurbine
            sowie Zugehoerige Erklaerungen in Test5.java

    Tobias: SolarThermalSystem, HeatPump
            sowie Zugehoerige Erklaerungen in Test5.java

     */



}
