/**
 * Created by Andreas Novak on 25.11.2017.
 */
public class Cable<A extends PowerSupply> extends Connection{

    private int litzen;

    public Cable(int litzen){
        super();
        this.litzen = litzen;
    }

    public Cable(int litzen, A tail, A head){
        super(head, tail);
        this.litzen = litzen;
    }

    // Nachbedingung: Die WaterSupply am Anfangspunkt wird zurueckgegeben (null, wenn nicht vorhanden).
    public Object source() {
        return getTail();
    }

    // Nachbedingung: Die PowerSupply am Endpunkt wird zurueckgegeben (null, wenn nicht vorhanden).
    public Object sink() {
        return getHead();
    }

    // Vorbedingung: litzen >= 0
    // Nachbedingung: Die Anzahl der Litzen wird zurueckgegeben.
    public int strands(){
        return litzen;
    }

    // Vorbedingung: litzen >= 0
    // Nachbedingung: Die Anzahl der Litzen wird als String zurueckgegeben.
    public String toString(){
        return litzen + "";
    }
}
