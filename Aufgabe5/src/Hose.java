/**
 * Created by Andreas Novak on 25.11.2017.
 */
public class Hose<A extends WaterSupply> extends Connection {

    private float durchmesser;

    public <A> Hose(float durchmesser){
        super();
        this.durchmesser = durchmesser;
    }

    public <A> Hose(float durchmesser, A tail, A head){
        super(tail, head);
        this.durchmesser = durchmesser;
    }

    // Nachbedingung: Die WaterSupply am Anfangspunkt wird zurueckgegeben (null, wenn nicht vorhanden).
    public A source() {
        return (A) getTail();
    }

    // Nachbedingung: Die WaterSupply am Endpunkt wird zurueckgegeben (null, wenn nicht vorhanden).
    public A sink() {
        return (A) getHead();
    }

    // Vorbedingung: durchmesser > 0
    // Nachbedingung: Der durchmesser wird zurueckgegeben.
    public float diameter(){
        return this.durchmesser;
    }

    // Vorbedingung: durchmesser > 0
    // Nachbedingung: Der durchmesser wird als String zurueckgegeben.
    public String toString(){
        return durchmesser + "";
    }

}
