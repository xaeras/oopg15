import java.util.Iterator;

/**
 * Created by Andreas Novak on 25.11.2017.
 */
public interface Ensemble<A> extends java.lang.Iterable<Object>{ // Object evntl aendern!

    //x != null
    //add's x to the end of the list (if the list is empty to the beginning)
    //if x isn't already in the list
    void add(A x);

    //returns an iterator over a set of elements
    Iterator iterator();

}
