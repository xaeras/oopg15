import java.util.Iterator;

/**
 * Created by Tobay on 27.11.2017.
 */
public class Composition<A,B extends Connection> extends Connection implements Ensemble{


    //Node != null
    private class Node {
        private Object elem;
        private Node next = null;
        private Node (Object elem){this.elem = elem;}
    }

    private Node tail; //first node of List
    private Node head; //last node of List

    private Node connectTail;
    private Node connectHead;

    @Override
    public Object sink() {return head;}

    @Override
    public Object source() {return tail;}

    //x != null
    //add's x to the end of the list (if the list is empty to the beginning)
    //if x isn't already in the list
    @Override
    public void add(Object x){
        if (this.tail == null){
            tail = new Node (x);
        }else if (this.head == null){
            tail.next = head = new Node (x);
        }else{
            head = head.next = new Node(x);
        }
    }

    //returns an iterator over a set of elements
    @Override
    public Iterator<Object> iterator() {
      return new Iter(tail);
    }

    private class Iter implements Iterator{

        private Node x;

        // Node != null
        Iter(Node x){
            this.x = x;
        }

        //returns true if the current iterator pointer x != null
        @Override
        public boolean hasNext() {
            return x != null;
        }

        //returns the current pointer x of the iterator ( null if x == null)
        //than the current pointer x is set to x.next
        @Override
        public Object next() {

            if (!hasNext()) {
                return null;
            }else{
                Object elem = x.elem;
                x = x.next;
                return elem;
            }
        }


        @Override
        public void remove() {
            if (x.equals(tail)){
                x = tail = x.next;
            }else if(x.equals(head)){
                x = null;
            }else {
                x = x.next;
            }
        }
    }

    //add's the element x with subtype of Connection to the end of the list
    public void connect(B x){

        add(x);

        if (this.connectTail == null){
            connectTail = new Node (x);
        }else if (this.connectHead == null){
            connectTail.next = connectHead = new Node (x);
        }else{
            connectHead = connectHead.next = new Node(x);
        }

        if (x.source() != null)//überprüfung ob noch nicht vorhanden fehlt
        {
            add(x.source());
        }
        if (x.sink() != null)//überprüfung ob noch nicht vorhanden fehlt
        {
            add(x.sink());
        }
    }

    public Iterator connectionIter(){
        return new Iter(connectTail);
    }
}
