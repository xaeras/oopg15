import java.util.Iterator;

/**
 * Created by marwinschindler on 28.11.17.
 */
public class Test5 {

    /* Arbeitsaufteilung:
     * Andreas Novak:
     * Connection-, Hose-, Cable-, Negator-Klasse.
     *
     * Tobias Ruttner:
     * Test-Klasse,  Connection-, Hose-, Cable-, Negator-Klasse.
     *
     * Marwin Schindler:
     * Composition-, Ensemble-Klasse
     */

    public static void main(String[] args) {

        //1.
        //erzeugen
        Ensemble<String> stringEnsemble = new Composition<>();
        Composition<Integer, Negator> compositionNegator = new Composition<>();
        Composition<WaterSupply, Hose> compositionWaterSupply = new Composition<>();
        Composition<PowerSupply, Cable> compositionPowerSupply = new Composition<>();


        Hose Hose1 = new Hose(1.0f);
        Hose Hose2 = new Hose(3.0f);
        Hose Hose3 = new Hose(5.0f);
        Cable Cable1 = new Cable(1);
        Cable Cable2 = new Cable(2);
        Cable Cable3 = new Cable(3);
        WaterSupply waterSupply1 = new WaterSupply();
        WaterSupply waterSupply2 = new WaterSupply();
        WaterSupply waterSupply3 = new WaterSupply();
        WaterSupply waterSupply4 = new WaterSupply();
        PowerSupply powerSupply1 = new PowerSupply();
        PowerSupply powerSupply2 = new PowerSupply();
        PowerSupply powerSupply3 = new PowerSupply();
        PowerSupply powerSupply4 = new PowerSupply();


        //einfügen
        compositionWaterSupply.add(waterSupply1);
        compositionWaterSupply.connect(Hose1);
        compositionWaterSupply.connect(Hose2);
        compositionWaterSupply.connect(Hose3);

        compositionPowerSupply.add(powerSupply1);
        compositionPowerSupply.add(powerSupply2);
        compositionPowerSupply.connect(Cable1);
        compositionPowerSupply.connect(Cable2);

        stringEnsemble.add("Ensemble");

        //ausgeben
        Iterator ownIter = compositionWaterSupply.connectionIter();

        System.out.println("Durchmesser der Hosen:");
        while (ownIter.hasNext()){
            System.out.println(ownIter.next().toString());
        }

        while (ownIter.hasNext()){
            System.out.println(ownIter.next().getClass().toString());
        }
        System.out.println();

        System.out.println("Anzahl der Litzen");
        ownIter = compositionPowerSupply.connectionIter();

        while (ownIter.hasNext()){
            System.out.println(ownIter.next().toString());
        }
        System.out.println();

        ownIter = stringEnsemble.iterator();

        //System.out.println("Ensemble");
        while (ownIter.hasNext()){
            System.out.println(ownIter.next().toString());
        }
        System.out.println();

        //entfernen

        Iterator ownIter2 = compositionWaterSupply.iterator();

        int i = 0;
        while (ownIter2.hasNext()){
            if (i == 0){
                ownIter2.remove();
            }
            System.out.println(ownIter2.next());
            i++;
        }

        ownIter2 = compositionPowerSupply.iterator();

        i = 0;
        while (ownIter2.hasNext()){
            if (i == 1){
                ownIter2.remove();
            }
            System.out.println(ownIter2.next());
            i++;
        }

        //neu einfügen
        compositionWaterSupply.add(waterSupply2);
        compositionPowerSupply.connect(Cable3);

        //erneut ausgeben
        ownIter = compositionWaterSupply.connectionIter();

        System.out.println("Neue Durchmesser der Hosen:");
        while (ownIter.hasNext()){
            System.out.println(ownIter.next().toString());
        }
        System.out.println();


        ownIter = compositionPowerSupply.connectionIter();

        System.out.println("Neue Anzahl der Litzen");
        while (ownIter.hasNext()){
            System.out.println(ownIter.next().toString());
        }
        System.out.println();

        ownIter = stringEnsemble.iterator();

        //System.out.println("Ensemble");
        while (ownIter.hasNext()){
            System.out.println(ownIter.next().toString());
        }
        System.out.println();

        //Summen bilden
        ownIter = compositionWaterSupply.connectionIter();
        float sumW = 0;
        while (ownIter.hasNext()){
            sumW += Float.parseFloat(ownIter.next().toString());
        }
        System.out.println("Summe Durchmesser: " + sumW);

        ownIter = compositionPowerSupply.connectionIter();
        float sumP = 0;
        while (ownIter.hasNext()){
            sumP += Float.parseFloat(ownIter.next().toString());
        }
        System.out.println("Summe Litzen: " + sumP);



        //2.
        //erzeugen
        Composition<Supply, Connection<Supply>> compositionConnection = new Composition<>();

        //auslesen und einfuegen
        ownIter = compositionWaterSupply.iterator();

        while (ownIter.hasNext()){
            if(ownIter.next() instanceof Supply){
                compositionConnection.add(ownIter.next());
            }
        }

        ownIter = compositionPowerSupply.connectionIter();

        while (ownIter.hasNext()){
            if(ownIter.next() instanceof Supply){
                compositionConnection.add(ownIter.next());
            }
        }

        ownIter = compositionConnection.iterator();

        while (ownIter.hasNext()){
            if(ownIter.next() instanceof Supply){
                System.out.println(ownIter.next().hashCode());
            }
        }

        //entfernen


        ownIter2 = compositionConnection.iterator();

        i = 0;
        while (ownIter2.hasNext()){
            if (i == 0){
                ownIter2.remove();
            }
            System.out.println(ownIter2.next());
            i++;
        }
        //neu einfügen
        compositionConnection.add(waterSupply3);
        compositionConnection.add(powerSupply3);


        //3.
        //erzeugen
        Ensemble<Supply> ensembleSupply = new Composition<>();

        ensembleSupply = compositionConnection;

        ensembleSupply.add(waterSupply1);

        //entfernen
        ownIter2 = ensembleSupply.iterator();

        i = 2;
        while (ownIter2.hasNext()){
            if (i == 2){
                ownIter2.remove();
            }
            System.out.println(ownIter2.next());
            i++;
        }

        //neu einfügen
        compositionConnection.add(waterSupply4);
        compositionConnection.add(powerSupply4);

        //ausgeben



    }

}
