
/**
 * Created by Andreas Novak on 25.11.2017.
 */
public abstract class Connection<A> {

    private A head = null;
    private A tail = null;

    public Connection(){
    }

    public Connection(A head, A tail){
        this.head = head;
        this.tail = tail;
    }


    public abstract A source();

    public abstract A sink();


    public A getHead() {
        return head;
    }

    public A getTail() {
        return tail;
    }

    public void setHead(A head){
        this.head = head;
    }

    public void setTail(A tail) {
        this.tail = tail;
    }

}
