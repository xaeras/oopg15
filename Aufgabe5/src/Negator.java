/**
 * Created by Andreas Novak on 25.11.2017.
 */
public class Negator extends Connection {

    public Negator(){}

    public Negator(int value){
        super(value, -value);
    }

    //setzt Wert der negiert wird
    public void setValue(int value){
        setTail(value);
        setHead(-value);
    }

    //gibt Wert am Anfangspunkt der Verbindung zurück.
    public Object source() {
        return getTail();
    }

    //gibt Wert am Endpunkt der Verbindung zurück, in diesem Fall das Inverse zum Wert am Anfangspunkt.
    public Object sink() {
        return getHead();
    }
}
