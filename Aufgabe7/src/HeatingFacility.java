/**
 * Created by andre on 11.12.2017.
 */
public class HeatingFacility extends Facility {


    //returns a short temperature range normal pipe x
    //inserts x, from the given warehouse y, into montiertePipes
    //inserts a big temperature range normal pipe instead if there are no small pipes left in y
    //removes the pipe from y
    //y != null
    @Override
    public Pipe montiere(Warehouse warehouse) {
        NormalPipe pipe = warehouse.montiereNormalPipe();
        if(pipe != null){
            this.montiertePipes.add(pipe);
        }
        return pipe;
    }
}
