import java.util.ArrayList;

/**
 * Created by andre on 11.12.2017.
 */
public class Test {

    /*
    Aufgabenverteilung:

    Andreas Novak: Warehouse und Pipe-Klassen, allgemeine Struktur.


    Tobias Ruttner: Testfaelle geschrieben + anlagenliste, lagerliste.


    Marwin Schindler: Facility-Klassen und Zusicherungen geschrieben.
     */


    /*
    allgemein: alle wichtigen Normal- und Grenzfaelle ueberpruefen und Ergebnisse in verstaendlicher Form darstellen.
    1. Instanzen aller Typen erzeugen
    2. Lager des Installateurs + Analgen mit mehreren Rohren erzeugen. (mindestens 3 unteschiedliche Anlagen.

    Es soll darauf geachtet werden, dass sich deklarierte Typen von Variablen im Allgemeinen von den dynamischen Typen ihrer Werte unterscheiden.
     */

    public static void main(String[] args) {
        Pipe littleAcidPipe1 = new LittleAcidPipe(350.5f, 340);
        Pipe littleAcidPipe2 = new LittleAcidPipe(240.3f, 300);
        Pipe littleAcidPipe3 = new LittleAcidPipe(180.9f, 220);

        Pipe bigAcidPipe1 = new BigAcidPipe(500.5f, 2000);
        Pipe bigAcidPipe2 = new BigAcidPipe(390.3f, 1500);
        Pipe bigAcidPipe3 = new BigAcidPipe(200.0f, 1000);

        Pipe littleNormalPipe1 = new LittleNormalPipe(120.7f, 179);
        Pipe littleNormalPipe2 = new LittleNormalPipe(250.0f, 290);
        Pipe littleNormalPipe3 = new LittleNormalPipe(375.5f, 321);

        Pipe bigNormalPipe1 = new BigNormalPipe(200.0f, 900);
        Pipe bigNormalPipe2 = new BigNormalPipe(326.4f, 1300);
        Pipe bigNormalPipe3 = new BigNormalPipe(480.5f, 1777);


        Warehouse warehouse = new Warehouse();

        Facility heatingFacility1 = new HeatingFacility();
        Facility heatingFacility2 = new HeatingFacility();

        Facility industryFacility1 = new IndustryFacility();
        Facility industryFacility2 = new IndustryFacility();

        warehouse.lagere(littleAcidPipe1);
        warehouse.lagere(littleAcidPipe2);
        warehouse.lagere(littleAcidPipe3);
        warehouse.lagere(bigAcidPipe1);
        warehouse.lagere(bigAcidPipe2);
        warehouse.lagere(bigAcidPipe3);
        warehouse.lagere(littleNormalPipe1);
        warehouse.lagere(littleNormalPipe2);
        warehouse.lagere(littleNormalPipe3);
        warehouse.lagere(bigNormalPipe1);
        warehouse.lagere(bigNormalPipe2);
        warehouse.lagere(bigNormalPipe3);

        System.out.println("Liste der Rohre im Lager:");
        System.out.println();

        warehouse.lagerliste();
        warehouse.lagerwert();

        heatingFacility1.montiere(warehouse);
        heatingFacility1.montiere(warehouse);
        heatingFacility2.montiere(warehouse);
        heatingFacility2.montiere(warehouse);
        industryFacility1.montiere(warehouse);
        industryFacility1.montiere(warehouse);
        industryFacility2.montiere(warehouse);
        industryFacility2.montiere(warehouse);

        System.out.println("\n");
        System.out.println("--- Einige Rohre wurden montiert ---");
        System.out.println("\n");

        System.out.println("Liste der uebrigen Rohre im Lager:" + "\n");

        warehouse.lagerliste();
        warehouse.lagerwert();

        System.out.println("\n");
        System.out.println("Liste der Rohre im ersten Heizungssystem:" + "\n");

        heatingFacility1.anlagenliste();
        heatingFacility1.anlagenwert();

        System.out.println("\n");
        System.out.println("Liste der Rohre im zweiten Heizungssystem:" + "\n");

        heatingFacility2.anlagenliste();
        heatingFacility2.anlagenwert();

        System.out.println("\n");
        System.out.println("Liste der Rohre im ersten Industriesystem:" + "\n");

        industryFacility1.anlagenliste();
        industryFacility1.anlagenwert();

        System.out.println("\n");
        System.out.println("Liste der Rohre im zweiten Industriesystem:" + "\n");

        industryFacility2.anlagenliste();
        industryFacility2.anlagenwert();



        heatingFacility1.montiere(warehouse);
        heatingFacility2.montiere(warehouse);
        industryFacility1.montiere(warehouse);
        industryFacility2.montiere(warehouse);

        System.out.println("\n");
        System.out.println("--- Weitere Rohre wurden montiert, Lager ist leer, letzte Montierung konnte nicht durchgefuehrt werden ---" + "\n");
        System.out.println("Lagerliste:");

        warehouse.lagerliste();

        System.out.println("--- Das erste Heizungssystem beinhaltet nach 4 Montierungsaufforderungen nur 3 Rohre ---" + "\n");

        heatingFacility1.anlagenliste();

        heatingFacility1.demontiere(warehouse);

        System.out.println("--- Ein Rohr aus dem ersten Heizungssystem wurde demontiert ---" +  "\n");

        System.out.println("Lagerliste:" + "\n");

        warehouse.lagerliste();

        System.out.println("Rohre im ersten Heizungssystem:" + "\n");

        heatingFacility1.anlagenliste();

        heatingFacility2.montiere(warehouse);

        System.out.println("--- Das Rohr wurde nun in das zweite Heizungssystem montiert ---" +  "\n");

        System.out.println("Lagerliste:");

        warehouse.lagerliste();

        System.out.println("Rohre im zweiten Heizungssystem:" + "\n");

        heatingFacility2.anlagenliste();

        heatingFacility1.demontiere(warehouse);
        heatingFacility1.demontiere(warehouse);
        heatingFacility1.demontiere(warehouse);
        heatingFacility1.demontiere(warehouse);
        heatingFacility2.demontiere(warehouse);
        heatingFacility2.demontiere(warehouse);
        heatingFacility2.demontiere(warehouse);
        heatingFacility2.demontiere(warehouse);
        industryFacility1.demontiere(warehouse);
        industryFacility1.demontiere(warehouse);
        industryFacility1.demontiere(warehouse);
        industryFacility1.demontiere(warehouse);
        industryFacility2.demontiere(warehouse);
        industryFacility2.demontiere(warehouse);
        industryFacility2.demontiere(warehouse);
        industryFacility2.demontiere(warehouse);

        System.out.println("--- Alle 12 Rohre wurden wieder ins Lager gelagert (Demontierungsaufruf konnte auch oefters als notwendig ausgerufen werden) ---" + "\n");

        System.out.println("Lagerliste:" + "\n");

        warehouse.lagerliste();
    }
}
