/**
 * Created by andre on 11.12.2017.
 */
public class IndustryFacility extends Facility {


    //returns a short temperature range acid pipe x
    //inserts x, from the given warehouse y, into montiertePipes
    //inserts a big temperature range acid pipe instead if there are no small pipes left in y
    //removes the pipe from y
    //y != null
    @Override
    public Pipe montiere(Warehouse warehouse) {
        AcidPipe pipe = warehouse.montiereAcidPipe();
        if(pipe != null){
            this.montiertePipes.add(pipe);
        }
        return pipe;

    }
}
