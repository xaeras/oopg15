/**
 * Created by andre on 11.12.2017.
 */
public abstract class Pipe {

    private float length;       // in cm
    private int price;          // in cents

    //length != 0
    public Pipe(float length, int price){
        this.length = length;
        this.price = price;
    }

    //returns a String containing length and price of the pipe
    public String getData(){
        String data =
                "Laenge (in cm): " + this.getLength() + "\n\r" +
                "Preis (in Cent): " + this.getPrice() + ";";
        return data;
    }

    //returns a String with type, temperature range, length and price of the pipe
    public abstract String getInfo();

    //inserts the pipe into the given warehouse
    //warehouse != null
    public abstract void lagere(Warehouse warehouse);

    public float getLength() {
        return length;
    }
    public int getPrice() {
        return price;
    }


    //TODO Wird das ueberhaupt benoetigt?
    public void setLength(float length) {
        this.length = length;
    }
    public void setPrice(int price) {
        this.price = price;
    }
}
