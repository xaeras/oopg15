import java.util.ArrayList;

/**
 * Created by andre on 11.12.2017.
 */
public abstract class Facility {
    public ArrayList<Pipe> montiertePipes = new ArrayList<>();

    //returns a small pipe x
    //inserts x, from the given warehouse y, into montiertePipes
    //inserts a big pipe instead if there are no small pipes left in y
    //removes the pipe from y
    //y != null
    public abstract Pipe montiere(Warehouse warehouse);

    //removes the first pipe from montiertePipes and inserts it into the given warehouse a
    //a != null
    public void demontiere(Warehouse warehouse){
        if(!montiertePipes.isEmpty()){
            warehouse.lagere(montiertePipes.get(0));
            montiertePipes.remove(0);
        }
    }

    //prints a String containing the total price of all pipes in montiertePipes
    //or a note if montiertePipes is empty
    public void anlagenwert(){
        if (!montiertePipes.isEmpty()) {
            String output = "Summe aller Preise der Rohre dieser Anlage betraegt (in Cent): ";
            int sum = 0;
            for (Pipe i : montiertePipes) {
                sum += i.getPrice();
            }
            System.out.println(output + sum + ".");
        }else{
            System.out.println("Keine Rohre in dieser Anlage");
        }
    }

    //prints a line for each pipe in montiertePipes, containing the associated informations
    //or a note if montiertePipes is empty
    public void anlagenliste(){
        if(!montiertePipes.isEmpty()) {
            String output = "";
            int counter = 1;
            for (Pipe i : montiertePipes) {
                output += "Rohr#" + counter + "\n" +
                        i.getInfo() + "\n\n";
                counter++;
            }
            System.out.println(output);
        }else {
            System.out.println("Keine Rohre in dieser Anlage");
        }
    }
}
