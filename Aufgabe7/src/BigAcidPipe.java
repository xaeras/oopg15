/**
 * Created by andre on 11.12.2017.
 */
public class BigAcidPipe extends AcidPipe {

    //length != 0
    public BigAcidPipe(float length, int price) {
        super(length, price);
    }

    //returns a String with type, temperature range, length and price of the pipe
    @Override
    public String getInfo() {
        return "Typ: Saeurebestaendiges Rohr mit großem Temperaturbereich,\n\r" + this.getData();
    }

    //inserts the pipe into the given warehouse
    //warehouse != null
    @Override
    public void lagere(Warehouse warehouse) {
        warehouse.lagereBigAcidPipe(this);
    }

}
