import java.util.ArrayList;

/**
 * Created by andre on 11.12.2017.
 */
public class Warehouse {
    // 4 Listen fuer verschiedene Rohrtypen
    private ArrayList<LittleAcidPipe> littleAcidPipes = new ArrayList<>();
    private ArrayList<BigAcidPipe> bigAcidPipes = new ArrayList<>();
    private ArrayList<LittleNormalPipe> littleNormalPipes = new ArrayList<>();
    private ArrayList<BigNormalPipe> bigNormalPipes = new ArrayList<>();

    //inserts a given pipe into a pipelist according to the pipes dynamic type
    public void lagere(Pipe pipe){pipe.lagere(this);}

    //inserts a short temperature range normal pipe into littleNormalPipes
    public void lagereLittleNormalPipe(LittleNormalPipe pipe){
        littleNormalPipes.add(pipe);
    }

    //inserts a big temperature range normal pipe into bigNormalPipes
    public void lagereBigNormalPipe(BigNormalPipe pipe){
        bigNormalPipes.add(pipe);
    }

    //inserts a short temperature range acid pipe into littleAcidPipes
    public void lagereLittleAcidPipe(LittleAcidPipe pipe){littleAcidPipes.add(pipe);}

    //inserts a big temperature range acid pipe into littleAcidPipes
    public void lagereBigAcidPipe(BigAcidPipe pipe){
        bigAcidPipes.add(pipe);
    }


    //returns a small temperature range acid pipe x
    //returns a big temperature range acid pipe instead if littleAcidPipes is empty
    //removes x from the according list
    //returns null instead if littleAcidPipes and bigAcidPipes are empty
    public AcidPipe montiereAcidPipe(){
        if(!littleAcidPipes.isEmpty()){
            AcidPipe acidPipe = littleAcidPipes.get(0);
            littleAcidPipes.remove(0);
            return acidPipe;
        }else if(!bigAcidPipes.isEmpty()){
            AcidPipe acidPipe = bigAcidPipes.get(0);
            bigAcidPipes.remove(0);
            return acidPipe;
        }
        return null;
    }

    //returns a small temperature range normal pipe x
    //returns a big temperature range normal pipe instead if littleNormalPipes is empty
    //removes x from the according list
    //returns null instead if littleNormalPipes and bigNormalPipes are empty
    public NormalPipe montiereNormalPipe(){
        if(!littleNormalPipes.isEmpty()){
            NormalPipe acidPipe = littleNormalPipes.get(0);
            littleNormalPipes.remove(0);
            return acidPipe;
        }else if(!bigNormalPipes.isEmpty()){
            NormalPipe acidPipe = bigNormalPipes.get(0);
            bigNormalPipes.remove(0);
            return acidPipe;
        }
        return null;
    }

    //prints the total price of all pipes in all lists of the warehouse in Cent
    //prints 0 if all lists are empty
    public void lagerwert(){
        String output = "Summe aller Preise der Rohre dieses Lagers betraegt (in Cent): ";
        int sum = 0;
        for(Pipe i : littleAcidPipes){
            sum += i.getPrice();
        }
        for(Pipe i : bigAcidPipes){
            sum += i.getPrice();
        }
        for(Pipe i : littleNormalPipes){
            sum += i.getPrice();
        }
        for(Pipe i : bigNormalPipes){
            sum += i.getPrice();
        }
        System.out.println(output + sum + ".");
    }

    //prints a line for each pipe in each list of the warehouse
    //containing the according informations
    //prints a note if all lists are empty
    public void lagerliste(){
        String output = "";
        int counter = 1;
        for(Pipe i : littleAcidPipes){
            output+= "Rohr#" + counter + "\n" +
                    i.getInfo() + "\n\n";
            counter++;
        }
        for(Pipe i : bigAcidPipes){
            output+= "Rohr#" + counter + "\n" +
                    i.getInfo() + "\n\n";
            counter++;
        }
        for(Pipe i : littleNormalPipes){
            output+= "Rohr#" + counter + "\n" +
                    i.getInfo() + "\n\n";
            counter++;
        }
        for(Pipe i : bigNormalPipes){
            output+= "Rohr#" + counter + "\n" +
                    i.getInfo() + "\n\n";
            counter++;
        }
        if (littleAcidPipes.isEmpty()&&bigAcidPipes.isEmpty()&&littleNormalPipes.isEmpty()&&bigNormalPipes.isEmpty()){
            output+="Keine Rohre in diesem Lager";
        }
        System.out.println(output);
    }
}
