/**
 * Created by Tobay on 04.12.2017.
 */


/*

Arbeitsaufteilung:

Andi: Container, House;

Marwin: Container, Room und House; die Zusicherungen zu diesen Klassen.

Tobi: Bulb und Lamp sowie deren Unterklassen; die Zusicherungen zu diesen Klassen.

*/

public class Test6 {
    public static void main(String[] args) {
        System.out.println("\t Aufbau eines Hauses: \n");

        House klHaus = new House("kleines Haus");

        Room Esszimmer = new Room("Esszimmer");
        LEDBulb aLED = new LEDBulb(50, 400.0);
        SimpleLamp a = new SimpleLamp(1,5,2, aLED);
        Esszimmer.add(a);
        Room Schlafzimmer = new Room("Schlafzimmer");
        klHaus.add(Esszimmer);
        klHaus.add(Schlafzimmer);

        System.out.println("1.\n" + klHaus.showRooms());

        Room Bad = new Room("Bad");
        LEDBulb bLED = new LEDBulb(100, 800);
        DimmableLamp aDimm = new DimmableLamp(2,100,100, bLED);
        SimpleLamp b = new SimpleLamp(3,300,0,aLED);
        Bad.add(aDimm);
        Bad.add(b);
        klHaus.add(Bad);
        LightBulb aBulb = new LightBulb(40, 100);
        LightBulb bBulb = new LightBulb(100, 200);
        SimpleLamp c = new SimpleLamp(4,200,1, aBulb);
        SimpleLamp d = new SimpleLamp(5,1000, 0,aBulb);
        DimmableLamp bDimm = new DimmableLamp(6,10,100,bBulb);
        Esszimmer.add(c);Esszimmer.add(d);Esszimmer.add(bDimm);
        Schlafzimmer.add(bDimm);
        Schlafzimmer.add(c);

        System.out.println("2.\n" + klHaus.showRooms());

        klHaus.remove(Bad);
        Esszimmer.remove(c);
        Schlafzimmer.remove(c);

        System.out.println("3. Remove 'Bad','4' \n" + klHaus.showRooms());

        Bad.remove(b);
        Room Lokus = new Room("Lokus");
        Lokus.add(c);
        klHaus.add(Lokus);
        klHaus.add(Bad);

        System.out.println("4. Nach kleiner Renovierung \n" + klHaus.showRooms());

        c.changeBulb(bLED);
        c.setDuration(100);
        aDimm.changeDimmDegree(50.5);
        d.setDuration(1);
        bDimm.changeBulb(aLED);
        bDimm.setDuration(1000);
        a.changeBulb(bBulb);
        a.setDuration(500);
        LEDBulb cLED = new LEDBulb(100,0.0);
        SimpleLamp badlampe = new SimpleLamp(7,0,0,cLED);
        Bad.remove(aDimm);
        Bad.add(badlampe);
        Bad.add(d);
        Esszimmer.add(c);
        bDimm.changeBulb(cLED);
        bDimm.setDuration(10000);
        Schlafzimmer.remove(bDimm);
        Schlafzimmer.add(bDimm);
        Bad.add(bDimm);

        House grHaus = new House("großes Haus");
        grHaus.add(Schlafzimmer);grHaus.add(Esszimmer);grHaus.add(Lokus);grHaus.add(Bad);
        Room Wohnzimmer = new Room("Wohnzimmer");
        Wohnzimmer.add(aDimm);
        Wohnzimmer.add(bDimm);
        Wohnzimmer.add(a);
        grHaus.add(Wohnzimmer);
        Room Terrasse = new Room("Terrasse");
        Terrasse.add(b);
        grHaus.add(Terrasse);

        System.out.println("5. Aendern von Lampen + Großes Haus\n" + klHaus.showRooms() + "\n" + grHaus.showRooms());

        System.out.println("6. Statistische Daten des kleinen Hauses\n");

        System.out.println(Esszimmer.avgRatingLedBulb());
        System.out.println(Schlafzimmer.avgRatingLedBulb());
        System.out.println(Bad.avgRatingLedBulb());
        System.out.println(Lokus.avgRatingLedBulb()+"\n");

        System.out.println(Esszimmer.avgRatingEazyDimm());
        System.out.println(Schlafzimmer.avgRatingEazyDimm());
        System.out.println(Bad.avgRatingEazyDimm());
        System.out.println(Lokus.avgRatingEazyDimm()+"\n");

        System.out.println(Esszimmer.avgOnTimeEazy());
        System.out.println(Schlafzimmer.avgOnTimeEazy());
        System.out.println(Bad.avgOnTimeEazy());
        System.out.println(Lokus.avgOnTimeEazy()+"\n");

        System.out.println(Esszimmer.avgOnTimeDimm());
        System.out.println(Schlafzimmer.avgOnTimeDimm());
        System.out.println(Bad.avgOnTimeDimm());
        System.out.println(Lokus.avgOnTimeDimm()+"\n");

        System.out.println(Esszimmer.avgLightOutput());
        System.out.println(Schlafzimmer.avgLightOutput());
        System.out.println(Bad.avgLightOutput());
        System.out.println(Lokus.avgLightOutput()+"\n");

        System.out.println(Esszimmer.maxTempBulb());
        System.out.println(Schlafzimmer.maxTempBulb());
        System.out.println(Bad.maxTempBulb());
        System.out.println(Lokus.maxTempBulb()+"\n");

        System.out.println(klHaus.usage());


   }

}
