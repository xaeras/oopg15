/**
 * Created by Tobay on 04.12.2017.
 */
public class SimpleLamp extends Lamp {

    private int color; // 0 = white, 1 = red, 2 = blue

    SimpleLamp(int id, int duration, int color) {
        super(id, duration);
        this.color = color;
    }

    SimpleLamp(int id, int duration, int color, Bulb bulb) {
        super(id, duration);
        this.color = color;
        this.setBulb(bulb);
    }


    //returns color of the lamp, -1 < int color < 3
    public String getColor() {

        if(this.color == 0){
            return "white";
        }

        if(this.color == 1){
            return "red";
        }

        if(this.color == 2){
            return "blue";
        }

        else {
            return "unknown color";
        }
    }


}
