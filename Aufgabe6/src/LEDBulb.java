/**
 * Created by Tobay on 04.12.2017.
 */
public class LEDBulb extends Bulb {

    private double lumen;

    LEDBulb(int watt, double lumen) {
        super(watt);
        this.lumen = lumen;
    }

    @Override
    //returns lumen of the LEDBulb, lumen >= 0;
    public double getLumen() {
        return this.lumen;
    }
}
