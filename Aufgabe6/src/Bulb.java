/**
 * Created by Tobay on 04.12.2017.
 */
public abstract class Bulb {

    private int watt;

    Bulb (int watt){
        this.watt = watt;
    }

    public int getWatt() {
        return this.watt;
    }

    //returns 0, gets overridden by LightBulb
    public int getMaxTemp() {
        return 0;
    }

    //returns 0, gets overridden by LEDBulb
    public double getLumen() {
        return 0;
    }

}
