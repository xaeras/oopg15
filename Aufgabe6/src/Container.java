import java.util.Iterator;

/**
 * Created by marwinschindler on 05.12.17.
 */
public abstract class Container{

    //add an Object x to the List
    //x != null
    public void add(Object x){
        if(head == null){
            tail = head = new Node(x);
        }else{
            tail = tail.next = new Node(x);
        }
    }

    //removes an Object x from the List
    //x must be in the Collection
    //x != null
    public void remove(Object x){
          if (x.equals(head.elem)) {
              head = head.next;
          } else if(x.equals(tail.elem)){
              Node i = head;
              while (x != i.next.elem){i = i.next;}
                  tail = i;
                  i.next = null;

          } else {
              Node i = head;
              while (x != i.next.elem){i = i.next;}
              i.next = i.next.next;
          }
    }

    //returns a ListIter if head != null
    public Iterator iterator(){return new ListIter();}

    private Node head = null;
    private Node tail = null;

    //elem != null
    private class Node {
        Node(Object elem){this.elem = elem;}
        Object elem;
        Node next = null;
    }

    private class ListIter implements Iterator {

        private Node pointer = head;

        //returns the current pointer
        //then sets the pointer to pointer.next
        public Object next(){
            if (pointer==null){
                return null;
            }
            Object elem = pointer.elem;
            pointer = pointer.next;
            return elem;
        }

        //returns true if the current pointer != null
        public boolean hasNext(){
            return pointer != null;
        }

    }

}
