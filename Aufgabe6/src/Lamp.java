import javafx.scene.effect.Light;

/**
 * Created by Tobay on 04.12.2017.
 */
public abstract class Lamp {

    private int id;
    private int duration;

    private Bulb bulb;

    Lamp (int id, int duration){
        this.id = id;
        this.duration = duration;
    }

    //returns ID of the lamp, id must be a unique number
    public int getId() {
        return this.id;
    }

    //returns duration of the lamp, duration >= 0
    public int getDuration() {
        return this.duration;
    }

    //changes duration of the lamp, newDuration >= 0
    public void setDuration (int newDuration){
        this.duration = newDuration;
    }

    //returns the actual bulb of the lamp
    public Bulb getBulb(){
        return this.bulb;
    }

    //sets a bulb for the lamp, lamp must not have a bulb before
    public void setBulb(Bulb bulb) {
        this.bulb = bulb;
    }

    //changes the bulb of the lamp and resets the duration
    public void changeBulb(LightBulb newBulb){
        this.bulb = newBulb;
        this.setDuration(0);
    }

    //creates a new bulb and changes it to the bulb of the lamp and resets the duration
    public void changeBulb(int watt, int maxTemp){
        this.bulb = new LightBulb(watt,maxTemp);
        this.setDuration(0);
    }

    //creates a new bulb and changes it to the bulb of the lamp and resets the duration
    public void changeBulb(LEDBulb newBulb){
        this.bulb = newBulb;
        this.setDuration(0);
    }

    //creates a new bulb and changes it to the bulb of the lamp and resets the duration
    public void changeBulb(int watt, double lumen){
        this.bulb = new LEDBulb(watt, lumen);
        this.setDuration(0);
    }



}
