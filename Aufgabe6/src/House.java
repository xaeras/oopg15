import java.util.Iterator;

/**
 * Created by marwinschindler on 05.12.17.
 */
public class House extends Container {

    final private String name;

    //name != null
    House(String name){
        this.name = name;
    }

    //returns a String containing all Rooms by name,
    //with their associated Lamps and their informations
    //if the House is empty, returns only its name
    public String showRooms(){
        String result = this.name + ": \n\n";

       for(Iterator it = this.iterator(); it.hasNext();){
           Room x = (Room) it.next();
           result += "\t" + x.getName() + ": \n";
           for (Iterator xt = x.iterator(); xt.hasNext();){
               Lamp y = (Lamp) xt.next();
               if (y instanceof SimpleLamp){
                   if (y.getBulb() instanceof LightBulb){
                       result+= "\t\t" + y.getId()  + " simple Gluehbirne " + ": " + y.getBulb().getWatt() + " Watt, maxTemp: " + y.getBulb().getMaxTemp() + ", Farbe: " + ((SimpleLamp) y).getColor() + "\n";
                   } else if(y.getBulb() instanceof LEDBulb){
                       result+=  "\t\t" + y.getId()  +  " simple LED " + ": " + y.getBulb().getWatt() + " Watt, " + y.getBulb().getLumen() + " Lumen, Farbe: " + ((SimpleLamp) y).getColor() + "\n";
                   }
               } else if (y instanceof DimmableLamp){
                   if (y.getBulb() instanceof LightBulb){
                       result+=  "\t\t" + y.getId() + " dimmbare Gluehbirne " + ": " + y.getBulb().getWatt() + " Watt, maxTemp: " + y.getBulb().getMaxTemp() + ", Dimmgrad: " + ((DimmableLamp) y).getDimmDegree() + "\n";
                   } else if(y.getBulb() instanceof LEDBulb){
                       result+=  "\t\t" + y.getId()  + " dimmbare LED " + ": " + y.getBulb().getWatt() + " Watt, " + y.getBulb().getLumen() + " Lumen, Dimmgrad: " + ((DimmableLamp) y).getDimmDegree() + "\n";
                   }
               }
           }
           result += "\n";
       }
       return result;
    }

    //returns total consumption of all lamps in a house, over their whole duration in kWh
    //if either duration or watt sum of all lamps == 0, returns 0
    public String usage() {
        int sum = 0; int sumDuration = 0;

        for (Iterator it = this.iterator(); it.hasNext();) {
            Room x = (Room) it.next();
            for (Iterator xt = x.iterator(); xt.hasNext(); ) {
                Lamp y = (Lamp) xt.next();
                sum += y.getBulb().getWatt();
                sumDuration += y.getDuration();
            }
        }
        return this.name + ": " + (sum/1000d)*(sumDuration/60d) + " kWh";
    }

    //returns the name
    public String getName(){
        return this.name;
    }
}
