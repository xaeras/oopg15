/**
 * Created by Tobay on 04.12.2017.
 */
public class DimmableLamp extends Lamp {

    private double dimmDegree;

    DimmableLamp(int id,int duration, double dimmDegree) {
        super(id, duration);
        this.dimmDegree = dimmDegree;
    }

    DimmableLamp(int id,int duration, double dimmDegree, Bulb bulb) {
        super(id, duration);
        this.dimmDegree = dimmDegree;
        this.setBulb(bulb);
    }

    //returns dimmDegree of the lamp, dimmdegree > 0
    public double getDimmDegree() {
        return this.dimmDegree;
    }

    //changes dimmDegree of the lamp, newDimmDegree > 0
    public void changeDimmDegree(double newDimmDegree){
        this.dimmDegree = newDimmDegree;
    }
}
