import java.util.Iterator;

/**
 * Created by marwinschindler on 05.12.17.
 */

public class Room extends Container {

    final private String name;

    Room(String name){this.name = name;}

    //returns a String containing the average power rating of all lamps in a room,
    //listed according to their bulb type
    //returns "keine typeA" if there are no Bulbs of typeA in the room
    public String avgRatingLedBulb(){
        int sumLed = 0; int countLed = 0;  int sumBulb = 0;  int countBulb = 0;

        for (Iterator it = this.iterator(); it.hasNext();){
            Lamp x = (Lamp) it.next();
            if (x.getBulb() instanceof LEDBulb) {
                sumLed+= x.getBulb().getWatt(); countLed++;
            } else if (x.getBulb() instanceof LightBulb){
                sumBulb+= x.getBulb().getWatt(); countBulb++;
            }
        }
        String result = this.name + ", Ø Nennleistung aller LEDlampen: \n";
        result += (countLed>0)?"\t" + sumLed/countLed:"keine LEDlampen";
        result += "\n";
        result += (countBulb>0)?"\t" + sumBulb/countBulb:"keine Gluehbirnen";
        return result;
    }

    //returns a String containing the average power rating of all lamps in a room,
    //listed according to their lamp type
    //returns "keine typeA Lampen" if there are no lamps of typeA in the room
    public String avgRatingEazyDimm(){
            int sumEazy = 0; int countEazy = 0; int sumDimm = 0; int countDimm = 0;

        for (Iterator it = this.iterator(); it.hasNext();){
            Lamp x = (Lamp)it.next();
            if (x instanceof SimpleLamp){
                sumEazy+=x.getBulb().getWatt();countEazy++;
            } else if (x instanceof DimmableLamp){
                sumDimm+=x.getBulb().getWatt();countDimm++;
            }
        }
        String result = this.name + ", Ø Nennleistung aller simplen Lampen: \n";
        result += (countEazy>0)? "\t" + sumEazy/countEazy:"keine simplen Lampen";
        result += "\n";
        result += (countDimm>0)? "\t" + sumDimm/countDimm:"keine dimmbaren Lampen";
        return result;
    }

    //returns a String containing the average on-time of all simple lamps in a room
    //listed according to their bulb type
    //returns "keine simplen typeA" if there are no bulbs of typeA
    public String avgOnTimeEazy(){
        int sumLed = 0; int countLed = 0; int sumBulb = 0; int countBulb = 0;

        for (Iterator it = this.iterator(); it.hasNext();){
            Lamp x = (Lamp) it.next();
            if (x instanceof SimpleLamp) {
                if (x.getBulb() instanceof LEDBulb) {
                    sumLed += x.getDuration(); countLed++;
                } else if (x.getBulb() instanceof LightBulb) {
                    sumBulb += x.getDuration(); countBulb++;
                }
            }
        }
        String result = this.name + ", Ø Einschaltdauer aller simplen LEDlampen: : \n";
        result += (countLed>0)?"\t" + sumLed/countLed:"keine simplen LEDlampen";
        result += "\n";
        result += (countBulb>0)?"\t" + sumBulb/countBulb:"keine simplen Gluehbirnen";
        return result;}


    //returns a String containing the average on-time of all dimmable lamps in a room
    //listed according to their bulb type
    //returns "keine dimmbaren typeA" if there are no bulbs of typeA
    public String avgOnTimeDimm(){
        int sumLed = 0; int countLed = 0; int sumBulb = 0; int countBulb = 0;

        for (Iterator it = this.iterator(); it.hasNext();){
            Lamp x = (Lamp) it.next();
            if (x instanceof DimmableLamp) {
                if (x.getBulb() instanceof LEDBulb) {
                    sumLed += x.getDuration(); countLed++;
                } else if (x.getBulb() instanceof LightBulb) {
                    sumBulb += x.getDuration(); countBulb++;
                }
            }
        }
        String result = this.name + ", Ø Einschaltdauer aller dimmbaren LEDlampen: \n";
        result += (countLed>0)?"\t" + sumLed/countLed:"keine dimmbaren LEDlampen";
        result += "\n";
        result += (countBulb>0)?"\t" + sumBulb/countBulb:"keine dimmbaren Gluehbirnen";
        return result;}

    //returns a String containing the average light output, for every lamp with a LED bulb in a room, as lumen/watt
    public String avgLightOutput(){
        String result = this.name + ": Ø Lichtausbeute pro Lampe:";
        for (Iterator it = this.iterator(); it.hasNext();){
            Lamp x = (Lamp) it.next();
            if(x.getBulb() instanceof LEDBulb){
                if (x.getBulb().getWatt()>0) {
                    result += "\t" + (x.getBulb().getLumen() / x.getBulb().getWatt());
                }
            }
        }
        return result;
    }

    //returns a String containing the maximum Temperature, for every lamp with a normal bulb in a room, in centigrade
    public String maxTempBulb(){
        String result = this.name + ": maxTemp pro Lampe:";
        for (Iterator it = this.iterator(); it.hasNext();) {
            Lamp x =(Lamp) it.next();
            if (x.getBulb() instanceof LightBulb){
                result+= "\t" + x.getBulb().getMaxTemp();
            }
        }
            return result;
    }

    //returns the name
    public String getName() {
        return name;
    }


}
