/**
 * Created by Tobay on 04.12.2017.
 */
public class LightBulb extends Bulb {

    private int maxTemp;

    LightBulb(int watt, int maxTemp) {
        super(watt);
        this.maxTemp = maxTemp;
    }

    @Override
    //returns max. Temperature of the Lightbulb, maxTemp >= 0;
    public int getMaxTemp() {
        return this.maxTemp;
    }
}
